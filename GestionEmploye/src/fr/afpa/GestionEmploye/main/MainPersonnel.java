package fr.afpa.GestionEmploye.main;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.Writer;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Scanner;

import fr.afpa.GestionEmploye.entities.Employe;
import fr.afpa.GestionEmploye.entities.EmployeProducer;
import fr.afpa.GestionEmploye.entities.EmployeProducerRiskIMP;
import fr.afpa.GestionEmploye.entities.EmployeWarehouseman;
import fr.afpa.GestionEmploye.entities.EmployeWarehousemanRiskIMP;
import fr.afpa.GestionEmploye.services.Personnel;

public class MainPersonnel {

	private static final String DELIMITER = ";";
	private static final String SEPARATOR = "\n";
	private static final String HEADER = "ID; Categorie; Nom et prenom; Age; Date d'entree; Salaire;";

	public static Scanner scanner = new Scanner(System.in);
	public static ArrayList<Employe> listEmployes = generateEmployes();
	public static Personnel personnelService = new Personnel();

	public static void main(String[] args) {

		String choice = "";
		boolean isRunning = true;

		do {

			menu();
			choice = scanner.nextLine();

			switch (choice) {
			case "1": {
				personnelService.createEmploye();
				break;
			}

			case "2": {
				personnelService.calculerSalairesaffich();
				break;
			}

			case "3": {
				System.out.println("Salaire moyen des employes : " + personnelService.salaireMoyen());
				break;
			}

			case "0": {
				saveAllData();
				System.out.println("fermeture l'application.");
				isRunning = false;
				break;
			}

			}
			if(!choice.equalsIgnoreCase("0") ){
			System.out.println("Appuyer sur une touche pour continuer... ");
			scanner.nextLine();
			}

		} while (isRunning);

	}

	private static void saveAllData() {

		String path = "./data/gestion-salaires.csv";
		
		File outPut = new File(path);
		
		outPut.getParentFile().mkdirs();

		try {
			
			Writer writer = new FileWriter(outPut);
			BufferedWriter br = new BufferedWriter(writer);
			br.write(HEADER);
			br.newLine();
			for (Employe employe : listEmployes) {				
				br.write(employe.getId() + DELIMITER);
				br.write(employe.getFullName() + DELIMITER);
				br.write(employe.getAge() + DELIMITER);
				br.write(employe.getAge() + DELIMITER);
				br.write(employe.getEntryDate() + DELIMITER);
				br.write(employe.getSalary() + DELIMITER);
				br.newLine();	
			}
			br.flush();
			br.close();
			System.out.println("Donnees sauvegardees dans " + path);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static ArrayList<Employe> generateEmployes() {
		ArrayList<Employe> generatedEmployes = new ArrayList<>();

		generatedEmployes.add(new EmployeProducer("nom" + 1, "prenom" + 1, 21, LocalDate.now(), 400));
		generatedEmployes.add(new EmployeProducerRiskIMP("nom" + 2, "prenom" + 2, 22, LocalDate.now(), 400));
		generatedEmployes.add(new EmployeWarehouseman("nom" + 3, "prenom" + 3, 23, LocalDate.now(), 200));
		generatedEmployes.add(new EmployeWarehousemanRiskIMP("nom" + 4, "prenom" + 4, 29, LocalDate.now(), 350));

		return generatedEmployes;
	}

	private static void menu() {
		System.out.println();
		System.out.println("=============================================");
		System.out.println("=	Tapez [1] pour ajouter un employe ");
		System.out.println();
		System.out.println("=	Tapez [2] pour Afficher le salaire de chaque employe");
		System.out.println();
		System.out.println("=	Tapez [3] pour Afficher le salaire moyen de tous les employes");
		System.out.println();
		System.out.println("=	Tapez [0] pour quitter l'application");
		System.out.println("=============================================");
		System.out.println();
	}

}
