package fr.afpa.GestionEmploye.services;

import java.time.LocalDate;

import fr.afpa.GestionEmploye.entities.Employe;
import fr.afpa.GestionEmploye.entities.EmployeProducer;
import fr.afpa.GestionEmploye.entities.EmployeProducerRiskIMP;
import fr.afpa.GestionEmploye.entities.EmployeRepresenting;
import fr.afpa.GestionEmploye.entities.EmployeSeller;
import fr.afpa.GestionEmploye.main.MainPersonnel;

public class Personnel implements IServiceEmploye {

	@Override
	public void createEmploye() {
		Employe employe = null;

		System.out.print("Veuiller saisir le nom de l'employe : " + "\n");
		String name = MainPersonnel.scanner.nextLine();

		System.out.print("Veuiller saisir le prenom de l'employe : " + "\n");
		String firstName = MainPersonnel.scanner.nextLine();

		System.out.print("Veuiller saisir  l'age  de l'employe : " + "\n");
		int age = Integer.parseInt(MainPersonnel.scanner.nextLine());

		System.out.print("Veuiller saisir la date d'entree de l'mploye ( dd-MM-yyyy ): " + "\n");
		LocalDate entryDate = LocalDate.parse(MainPersonnel.scanner.nextLine(), Employe.formatter);

		String choixSeq;
		do {
			System.out.println("taper 1 => creer un Seller ");
			System.out.println("taper 2 => creer un Representing ");
			System.out.println("taper 3 => creer un Producer");
			System.out.println("taper 4 => creer un ProducerRisk");
			System.out.println("taper 5 => creer un Warehouseman");
			System.out.println("taper 6 => creer un WarehousemanRisk");
			choixSeq = MainPersonnel.scanner.nextLine();
		} while (!("1".equals(choixSeq)) && !("2".equals(choixSeq)) && !("3".equals(choixSeq))
				&& !("4".equals(choixSeq)) && !("5".equals(choixSeq)) && !("6".equals(choixSeq)));

		if ("1".equals(choixSeq) || "2".equals(choixSeq)) {
			System.out.print("Veuiller saisir le chiffre d'affaire :");
			double turnover = Double.parseDouble(MainPersonnel.scanner.nextLine());
			if ("1".equals(choixSeq)) {
				employe = new EmployeSeller(name, firstName, age, entryDate, turnover);

			} else {
				employe = new EmployeRepresenting(name, firstName, age, entryDate, turnover);
			}
		}

		else if ("3".equals(choixSeq) || "4".equals(choixSeq)) {
			System.out.print("Veuiller saisir le nombre d'unites produites mensullement :");
			int unitsProducedMonthly = Integer.parseInt(MainPersonnel.scanner.nextLine());
			if ("3".equals(choixSeq)) {
				employe = new EmployeProducer(name, firstName, age, entryDate, unitsProducedMonthly);
			} else {
				employe = new EmployeProducerRiskIMP(name, firstName, age, entryDate, unitsProducedMonthly);
			}
		} else if ("5".equals(choixSeq) || "6".equals(choixSeq)) {
			System.out.print("Veuiller saisir le nombre d'heures travaillées mensullement :");
			int hours = Integer.parseInt(MainPersonnel.scanner.nextLine());
			if ("5".equals(choixSeq)) {
				employe = new EmployeProducerRiskIMP(name, firstName, age, entryDate, hours);
			} else {
				employe = new EmployeProducerRiskIMP(name, firstName, age, entryDate, hours);
			}
		}
		System.out.println("l'employe a bien ete cree");
		MainPersonnel.listEmployes.add(employe);
	}

	@Override
	public void calculerSalairesaffich() {
		System.out.printf("%-45s\t%s\t", "Employe", "Salaire");
		System.out.print(System.lineSeparator());
		for (Employe employe : MainPersonnel.listEmployes) {
			employe.calcSalary();
			System.out.printf("%-45s\t%s\n", employe.getFullName(), employe.getSalary() + "E");
		}
	}

	@Override
	public double salaireMoyen() {
		Double sum = 0.0;

		for (Employe employe : MainPersonnel.listEmployes) {
			employe.calcSalary();
			sum += employe.getSalary();
		}
		Double medium = sum / MainPersonnel.listEmployes.size();

		return medium;
	}
}
