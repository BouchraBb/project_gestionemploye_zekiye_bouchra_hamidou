package fr.afpa.GestionEmploye.entities;

import java.time.LocalDate;

public class EmployeProducer extends Employe {
	
	private Integer nProduction;	
	
	public EmployeProducer() {
		super();
	}

	public EmployeProducer(String name, String firstName, Integer age, LocalDate entryDate, Integer nProduction) {
		super(name, firstName, age, entryDate);
		this.nProduction = nProduction;
	}

	public double getnProduction() {
		return nProduction;
	}

	public void setnProduction(Integer nProduction) {
		this.nProduction = nProduction;
	}

	@Override
	public double calcSalary() {
		super.setSalary((double) (this.nProduction * 5));
		return super.getSalary();
	}

}
