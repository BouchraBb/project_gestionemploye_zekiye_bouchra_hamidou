package fr.afpa.GestionEmploye.entities;

import java.time.LocalDate;

import fr.afpa.GestionEmploye.services.IServiceRisk;

public class EmployeWarehousemanRiskIMP extends EmployeWarehouseman implements IServiceRisk {

	public EmployeWarehousemanRiskIMP() {
		super();
	}

	public EmployeWarehousemanRiskIMP(String name, String firstName, Integer age, LocalDate entryDate, Integer hours) {
		super(name, firstName, age, entryDate, hours);

	}

	@Override
	public double calcSalary() {
		return addPrimeRisk(super.calcSalary());
	}

	@Override
	public Double addPrimeRisk(Double salary) {
		super.setSalary(salary + RISK_PRIM);
		return super.getSalary();
	}
}
