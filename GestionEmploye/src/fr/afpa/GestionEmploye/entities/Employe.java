package fr.afpa.GestionEmploye.entities;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public abstract class Employe {

	
	public static DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");
	private static int intId = 0;
	
	protected Integer id;
	protected String name;
	protected String firstName;
	protected Integer age;
	protected LocalDate entryDate;
	protected Double salary;

	public Employe() {
		super();
		this.id = ++intId;
	}

	public Employe(String name, String firstName, Integer age, LocalDate entryDate) {
		super();
		this.id = ++intId;
		this.name = name;
		this.firstName = firstName;
		this.age = age;
		this.entryDate = entryDate;
	}

	public Integer getId() {
		return id;
	}

	public String getName() {
		return name.toUpperCase();
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public Integer getAge() {
		return age;
	}

	public void setAge(Integer age) {
		this.age = age;
	}

	public LocalDate getEntryDate() {
		return entryDate;
	}

	public void setEntryDate(LocalDate entryDate) {
		this.entryDate = entryDate;
	}

	public String getFullName() {
		return this.getClass().getSimpleName()+" "+ getName() + " " + this.firstName;
	}

	public abstract double calcSalary();	

	public Double getSalary() {
		return salary;
	}

	public void setSalary(Double salary) {
		this.salary = salary;
	}

	@Override
	public String toString() {
		return "Employe ["
				+ "id=" + this.id +"\n"
				+ " name=" + this.name +"\n"
				+ " firstName=" + this.firstName +"\n"
				+ " age=" + this.age +"\n"
				+ " entryDate="+ formatter.format(this.entryDate) + "]";
	}

}
