package fr.afpa.GestionEmploye.entities;

import java.time.LocalDate;

public class EmployeWarehouseman extends Employe {

	private Integer hours;

	public EmployeWarehouseman() {
		super();
	}

	public EmployeWarehouseman(String name, String firstName, Integer age, LocalDate entryDate, Integer hours) {
		super(name, firstName, age, entryDate);
		this.hours = hours;
	}

	public double getHours() {
		return hours;
	}

	public void setHours(Integer hours) {
		this.hours = hours;
	}

	@Override
	public double calcSalary() {
		setSalary((double) (this.hours * 5));
		return super.salary;
	}

}
