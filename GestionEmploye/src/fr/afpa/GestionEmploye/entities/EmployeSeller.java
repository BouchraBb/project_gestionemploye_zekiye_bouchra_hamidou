package fr.afpa.GestionEmploye.entities;

import java.time.LocalDate;

public class EmployeSeller extends EmployeMarketing {
	private final static Double bonus = 400d;
	protected Double turnover;

	public EmployeSeller() {
		super();
	}

	public EmployeSeller(String name, String firstName, Integer age, LocalDate entryDate, Double turnover) {
		super(name, firstName, age, entryDate,turnover);
		
	}

	@Override
	public double calcSalary() {
		super.setSalary(super.calcSalary() + bonus);
		return super.getSalary();

	}

	@Override
	public String toString() {
		return "  " + this.getClass() + " [" + "id=" + this.id + "\n" + " name=" + this.name + "\n" + " firstName="
				+ this.firstName + "\n" + " age=" + this.age + "\n" + " entryDate=" + formatter.format(this.entryDate)
				+ "\n" + " turnover=" + this.turnover + "]";
	}

}
