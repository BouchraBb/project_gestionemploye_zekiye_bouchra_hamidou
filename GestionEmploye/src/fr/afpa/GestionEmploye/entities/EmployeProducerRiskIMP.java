package fr.afpa.GestionEmploye.entities;

import java.time.LocalDate;

import fr.afpa.GestionEmploye.services.IServiceRisk;

public class EmployeProducerRiskIMP extends EmployeProducer implements IServiceRisk {

	public EmployeProducerRiskIMP() {
		super();
	}

	public EmployeProducerRiskIMP(String name, String firstName, Integer age, LocalDate entryDate,
			Integer nProduction) {
		super(name, firstName, age, entryDate, nProduction);
	}

	@Override
	public double calcSalary() {
		return addPrimeRisk(super.calcSalary());
	}

	@Override
	public Double addPrimeRisk(Double salary) {
		super.setSalary(super.getSalary() + RISK_PRIM);
		return super.getSalary();
	}

}
