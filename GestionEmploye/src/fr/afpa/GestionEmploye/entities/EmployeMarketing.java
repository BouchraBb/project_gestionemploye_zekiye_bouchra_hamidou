package fr.afpa.GestionEmploye.entities;

import java.time.LocalDate;

public abstract class EmployeMarketing extends Employe {

	protected Double turnover;

	public EmployeMarketing() {
		super();
	}

	public EmployeMarketing(String name, String firstName, Integer age, LocalDate entryDate, Double turnover) {
		super(name, firstName, age, entryDate);
		this.turnover = turnover;

	}

	public Double getTurnover() {
		return turnover;
	}

	public void setTurnover(Double turnover) {
		this.turnover = turnover;
	}

	@Override
	public double calcSalary() {
		super.setSalary(this.turnover * 0.2);
		return super.getSalary();
	}

}
